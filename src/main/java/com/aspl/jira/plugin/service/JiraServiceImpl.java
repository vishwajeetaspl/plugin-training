package com.aspl.jira.plugin.service;
import com.aspl.jira.plugin.service.JiraService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class JiraServiceImpl implements JiraService {

    @Override
    public List<Project> allProjects() {

        List<Project> projects= ComponentAccessor.getProjectManager().getProjects();

        return projects;
    }

    @Override
    public List<ApplicationUser> allUsers() {
        Collection<ApplicationUser> users = ComponentAccessor.getUserUtil().getUsers();

        List<ApplicationUser> userList = new ArrayList<>();
        for(ApplicationUser user : users){
            userList.add(user);
        }

        return userList;
    }
}
