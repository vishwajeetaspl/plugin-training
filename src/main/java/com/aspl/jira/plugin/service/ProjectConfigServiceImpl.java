package com.aspl.jira.plugin.service;


import com.aspl.jira.plugin.ao.dao.ProjetConfigDAO;
import com.aspl.jira.plugin.ao.entity.ProjectConfEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectConfigServiceImpl implements ProjectConfigService{

    @Autowired
    ProjetConfigDAO projetConfigDAO;



    @Override
    public List<ProjectConfEntity> allProjectConfig() {
        List<ProjectConfEntity> configData = new ArrayList<>();

        for(ProjectConfEntity projectConfEntity : projetConfigDAO.getAllProjectConfig()){
            configData.add(projectConfEntity);
        }

        return configData;
    }


    @Override
    public void saveProjectConfig(String projectKey, String userName) {


        projetConfigDAO.saveProjectConfig(projectKey, userName);


    }
}
