package com.aspl.jira.plugin.service;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

import java.util.List;

public interface JiraService {

    List<Project> allProjects();
    List<ApplicationUser> allUsers();

}
