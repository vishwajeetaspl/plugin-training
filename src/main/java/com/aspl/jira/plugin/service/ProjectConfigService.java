package com.aspl.jira.plugin.service;

import com.aspl.jira.plugin.ao.entity.ProjectConfEntity;

import java.util.List;

public interface ProjectConfigService {
   List<ProjectConfEntity> allProjectConfig();

    void saveProjectConfig(String projectKey, String userName);
}
