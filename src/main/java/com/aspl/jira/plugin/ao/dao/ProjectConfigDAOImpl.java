package com.aspl.jira.plugin.ao.dao;

import com.aspl.jira.plugin.ao.entity.ProjectConfEntity;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectConfigDAOImpl implements ProjetConfigDAO{

    @ComponentImport
    @Autowired
    private ActiveObjects ao;

    @Override
    public void saveProjectConfig(String projectKey, String userName) {
        ProjectConfEntity projectConfEntity = ao.create(ProjectConfEntity.class);
        projectConfEntity.setProjectKey(projectKey);
        projectConfEntity.setUserName(userName);
        projectConfEntity.save();

    }

    @Override
    public ProjectConfEntity[] getAllProjectConfig()
    {
        System.out.println(" getAllRecords()");


        ProjectConfEntity[]  queryResult =  ao.find(ProjectConfEntity.class, Query.select().where("PROJECT_KEY = ? ", "TP" ));

        System.out.println(" queryResult ");
        System.out.println(queryResult);







        //Query.select().where("PROJECT_KEY = ? AND ID = ?", pKey, fieldMappingId )


        return queryResult;
    }





}
