package com.aspl.jira.plugin.ao.dao;

import com.aspl.jira.plugin.ao.entity.ProjectConfEntity;

public interface ProjetConfigDAO {
    void saveProjectConfig(String projectKey, String userName);
    ProjectConfEntity[] getAllProjectConfig();
}
