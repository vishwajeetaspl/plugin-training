package com.aspl.jira.plugin.ao.entity;

import net.java.ao.Entity;

public interface ProjectConfEntity extends Entity {

    String getProjectKey();

    void setProjectKey(String pKey);

    String getUserName();

    void setUserName(String userName);

}
