package com.aspl.jira.plugin.actions;

import com.aspl.jira.plugin.ao.entity.ProjectConfEntity;
import com.aspl.jira.plugin.service.JiraService;
import com.aspl.jira.plugin.service.ProjectConfigService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public class ProjectConfigAction extends JiraWebActionSupport {


    @Autowired
    JiraService jiraService;

    @Autowired
    ProjectConfigService projectConfigService;

    private String user;
    private Date date;

    private List<Project> projects;
    private List<ApplicationUser> users;

    private List<ProjectConfEntity> configData;









    @Override
    public String execute() throws Exception {

        System.out.println("ProjectConfigAction execute");

        setUser("sunil");
        setDate(new Date());


        List<Project> projects = jiraService.allProjects();
        setProjects(projects);

        List<ApplicationUser> users =  jiraService.allUsers();
        setUsers(users);
        //this.projects = projects;

        List<ProjectConfEntity> configData = projectConfigService.allProjectConfig();
        setConfigData(configData);




        return "input";
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public JiraService getJiraService() {
        return jiraService;
    }

    public void setJiraService(JiraService jiraService) {
        this.jiraService = jiraService;
    }

    public List<ApplicationUser> getUsers() {
        return users;
    }

    public void setUsers(List<ApplicationUser> users) {
        this.users = users;
    }

    public List<ProjectConfEntity> getConfigData() {
        return configData;
    }

    public void setConfigData(List<ProjectConfEntity> configData) {
        this.configData = configData;
    }
}
