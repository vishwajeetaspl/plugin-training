package com.aspl.jira.plugin.actions;

import com.aspl.jira.plugin.service.JiraService;
import com.aspl.jira.plugin.service.ProjectConfigService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AddProjectConfigAction  extends JiraWebActionSupport {

    private String project;
    private String jirauser;

    private List<Project> projects;
    private List<ApplicationUser> users;

    @Autowired
    JiraService jiraService;

    @Autowired
    ProjectConfigService projectConfigService;

    @Override
    public String doExecute() throws Exception {
        System.out.println("AddProjectConfigAction execute");


        System.out.println("Project : "+getProject());
        System.out.println("UserName : "+getJirauser());

        projectConfigService.saveProjectConfig(getProject(), getJirauser() );

        return getRedirect("http://localhost:2990/jira/secure/ProjectConfigAction.jspa");

    }

    @Override
    protected void doValidation()
    {
        System.out.println("AddProjectConfigAction doValidation");

        if(project == null || project.equals("null") )
            addErrorMessage("Project not selected");

        if(jirauser == null || jirauser.equals("null"))
            addErrorMessage("User not selected");


        List<Project> projects = jiraService.allProjects();
        setProjects(projects);

        List<ApplicationUser> users =  jiraService.allUsers();
        setUsers(users);

    }






    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getJirauser() {
        return jirauser;
    }

    public void setJirauser(String jirauser) {
        this.jirauser = jirauser;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<ApplicationUser> getUsers() {
        return users;
    }

    public void setUsers(List<ApplicationUser> users) {
        this.users = users;
    }
}
