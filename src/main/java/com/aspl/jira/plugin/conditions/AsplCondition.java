package com.aspl.jira.plugin.conditions;

import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

public class AsplCondition extends AbstractWebCondition {

    @Override
    public void init(Map params) throws PluginParseException {
        System.out.println("###PARAMS : "+params);

        super.init(params);
    }


    @Override
    public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {

        if(applicationUser.getUsername().equals("sunil"))
            return true;

        return false;
    }
}
