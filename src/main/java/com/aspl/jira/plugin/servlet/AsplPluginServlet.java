package com.aspl.jira.plugin.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.velocity.exception.VelocityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class AsplPluginServlet extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(AsplPluginServlet.class);

    @ComponentImport
    @Autowired
    TemplateRenderer templateRenderer;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {

        resp.setContentType("text/html");
        //resp.getWriter().write("<html><body>Hello World</body></html>");

        Map<String, Object> velocityParams = new HashMap<>();

        ApplicationUser curreUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();


        velocityParams.put("user",curreUser);
        velocityParams.put("todaysdate", new Date());


        StringWriter writer = new StringWriter();
        try {
           // templateRenderer.render("templates/config.vm", writer);

            templateRenderer.render("templates/config.vm", velocityParams, writer);


        } catch (RenderingException | IOException e) {
            throw new VelocityException(e);
        }

        //resp.getWriter().write("<html><body>Hello World</body></html>");

        resp.getWriter().write(writer.toString());
    }

}